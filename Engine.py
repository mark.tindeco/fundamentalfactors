# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 11:04:00 2019

@author: Norberto
"""

import numpy as np, pandas
import matplotlib.pyplot as plt
import FactorModelCalibration, FactorModel, GbmCalibration, GBM

class globalCalibration():
    
    def __init__(self):
        self._factor_calibration =  FactorModelCalibration.calibrateFactorModel()
        indexcal = GbmCalibration.GbmCalibration(self._factor_calibration._indexreturns,GbmCalibration.DataType.Return)
        self._index_calibration = indexcal.calibrate()
        self._globalCorrelation = self._factor_calibration._correlation
        self._globalDim = self._globalCorrelation.shape[0]

class shockProvider():
    def generateShocks(dim, correlation, trials):
        while True:
            yield np.split(np.random.multivariate_normal(np.zeros(dim),correlation, trials),[1],axis=1)
        
    def generateIndepedentShocks(dim, trials):
        while True:
            yield np.random.multivariate_normal(np.zeros(dim),np.diag(np.ones(dim)), trials)
        
def globalSimulation(delta,numberOfSteps,trials,factorModel:FactorModel.FactorModel,indexModel:GBM.GBM,corrShockGen,indShockGen):
    factorModelSim = np.ones(shape=(factorModel._assetdim,trials))
    indexSim = np.ones(trials)
    assetSim = [pandas.DataFrame(factorModelSim * indexSim, index = factorModel._modeldrifts.index)]
    for j in range(numberOfSteps):
        indexShocks, factorShocks = next(corrShockGen)
        idiosyncraticShocks = next(indShockGen)
        factorModelSim = factorModel.stepUpdate(factorModelSim,delta,factorShocks,idiosyncraticShocks)
        indexSim = indexModel.stepUpdate(indexSim,delta,indexShocks[0])
        assetSim.append(factorModelSim * indexSim)
    return assetSim
        
def Run(delta,numberOfSteps,trials):
    globalCal = globalCalibration()
    factorModel = FactorModel.FactorModel(globalCal._factor_calibration)
    indexModel = GBM.GBM(globalCal._index_calibration)
    correlatedShocks = shockProvider.generateShocks(globalCal._globalDim,globalCal._globalCorrelation,trials)
    independentShocks = shockProvider.generateIndepedentShocks(factorModel._assetdim,trials)
    sim = globalSimulation(delta,numberOfSteps,trials,factorModel,indexModel,correlatedShocks,independentShocks)
    return sim    

def plotSingleAssetSim(sim,j):
    plt.plot(np.array([x.iloc[j] for x in sim]))#plt.plot(np.array([x[j,0:] for x in sim]).T)
    
def scatterPlotSimAtStep(sim,step,j):
    plt.scatter(np.arange(len(sim[step].columns)),sim[step].iloc[j])#plt.scatter(np.arange(len(sim)),np.array([x[:,step] for x in sim])[:,j])
