import pandas as pd
import matplotlib.pyplot as plt

markersize = 5

class Asset:
    
    def __init__(self, asset_id, database):
        self.id = asset_id

        query = """SELECT `assets`.*, `code`
        FROM `assets`
        LEFT JOIN `lookup_currencies` ON `assets`.`currency_id`=`lookup_currencies`.`currency_id`
        WHERE `asset_id`={}""".format(self.id)
        self.asset = database.fetch_result(query)
        self.database = database
        
    def refresh(self):
        """Refreshes information on the asset from the database."""
        
    @property
    def name(self): return self.asset['name']
    
    @property
    def ric(self): return self.asset['RIC']

    @property
    def currency(self): return self.asset['code']

    @property
    def first_trade_date(self): return self.asset['first_trade_date']

    @property
    def start_date(self): return self.asset['start_date']

    @start_date.setter
    def start_date(self, start_date):
        self.asset['start_date'] = start_date
        self.database.update_start_date(self.id, start_date)

    @property
    def end_date(self): return self.asset['end_date']
    
    @end_date.setter
    def end_date(self, end_date):
        self.asset['end_date'] = end_date
        self.database.update_end_date(self.id, end_date)

    def save_prices(self, prices):
        self.database.save_daily_close_prices(prices)

    def plot_prices(self, table):
        prices = self.database.get_daily_close_prices(self.id, wide=True)
        if len(prices.index) == 0: print("Nothing to plot."); return
        prices.plot()
        plt.title("{}\nClose Prices ({})".format(self.name, self.currency))
    
    def get_all_prices(self, adjusted=False):
        all_prices = self.database.get_daily_close_prices(self.id, wide=True, adjusted=adjusted)
        if all_prices is None: return None
        all_prices = all_prices.droplevel('source_id', axis='columns').rename(mapper={self.id: self.ric}, axis='columns')
        all_prices['day_of_week'] = all_prices.apply(lambda x: x.name.strftime("%A"), axis=1)
        query = "SELECT `date`, `holiday` FROM `lookup_holidays` WHERE `exchange_id`={}".format(self.asset['exchange_id'])
        holidays = pd.DataFrame(self.database.fetch_results(query)).set_index('date')
        all_prices = pd.merge(all_prices, holidays, how='left', left_index=True, right_index=True).fillna('')
        return all_prices
    
    def plot(self, adjusted=False):
        all_prices = self.get_all_prices(adjusted=adjusted)
        if all_prices is None: print("No prices to plot for", self.name); return
        plt.figure(num=self.id)
        ax = plt.subplot(111)
        all_prices.plot(ax=ax)
        markers = ['None', '+', 'x', 'o']
        linestyles = ['-', 'none', 'none', 'none']
        markerfacecolors = ['none', 'none', 'none', 'none']
        markeredgecolors = ['black', 'blue', 'red', 'grey']
        for i, line in enumerate(ax.get_lines()):
            line.set_linestyle(linestyles[i])
            line.set_marker(markers[i])
            line.set_markerfacecolor(markerfacecolors[i])
            line.set_markeredgecolor(markeredgecolors[i])
        ax.legend()
        plt.title("{}\nClose Prices ({})".format(self.name, self.currency))
        
    def adjustments(prices_unadjusted, prices_adjusted):
        adjustment_series = pd.Series(name='adjustment')
        if prices_unadjusted is None: return adjustment_series
        prices_merged = pd.merge(prices_unadjusted.rename(columns = {'price': 'unadjusted'}), prices_adjusted.rename(columns = {'price': 'adjusted'}), left_index=True, right_index=True)
        prices_merged = prices_merged.droplevel(['source_id', 'asset_id'])
        for index in range(1, len(prices_merged)):
            row = prices_merged.iloc[index - 1]
            last_close, last_adjusted_close = row['unadjusted'], row['adjusted']
            row = prices_merged.iloc[index]
            close, adjusted_close = row['unadjusted'], row['adjusted']
            date = row.name
            close_return = close / last_close
            adjusted_close_return = adjusted_close / last_adjusted_close
            adjustment = close_return / adjusted_close_return
            if abs(adjustment - 1) > 0.000001:
                adjustment_series[date] = adjustment
        return adjustment_series

    def save_adjustments(self, adjustment_series):
        for date, adjustment in adjustment_series.iteritems():
            query = "INSERT INTO `adjustments` SET `asset_id`={}, `date`='{}', `adjustment`={}".format(self.id, date, adjustment)
            try: self.database.execute(query)
            except: print("There's already an adjustment on {} for asset_id {}".format(date, self.id))

    def save_corax_adjustments(self, corax_adjustments):
        for index, row in corax_adjustments.iterrows():
            date, ric = index
            adjustment = row['adjustment']
            adjustment_type = row['adjustment_type']
            query = "INSERT INTO `corporate_actions` SET `asset_id`={}, `date`='{}', `adjustment`={}, `adjustment_type`='{}'".format(self.id, date, adjustment, adjustment_type)
            try: self.database.execute(query)
            except: print("There's already an adjustment on {} for asset_id {}".format(date, self.id))
