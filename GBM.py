# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 14:14:08 2019

@author: Norberto
"""

import numpy as np
import GbmCalibration

class GBM:
    
    def __init__(self,calibration:GbmCalibration.GbmCalibrationResult):
        self._drift = calibration._drift
        self._vol = calibration._vol
        self._adjusted_drift = self._drift - 0.5 * self._vol * self._vol      
        
    def logReturn(self,delta,shock):
        return self._adjusted_drift * delta + self._vol * np.sqrt(delta) * shock
        
    def stepUpdate(self,s,delta,shock):
        return s * np.exp(self.logReturn(delta,shock))
    
    def simPath(self,s,delta,numberOfSteps,shocks):
        x = np.empty((1,numberOfSteps + 1))
        x[:,0] = s
        y = s
        for j in range(numberOfSteps):
            y = self.stepUpdate(y,delta,shocks[j])
            x[:,j + 1] = y
        return x
        