# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 12:19:26 2019

@author: Norberto
"""

import numpy as np
from enum import Enum

class DataType(Enum):
    Price = 1
    Return = 2

class GbmCalibrationResult:
    
    def __init__(self,drift,vol):
        self._drift = drift
        self._vol = vol
        
class GbmCalibration:
    
    def __init__(self,data,datatype:DataType):
        self._data = data
        self._datatype = datatype
        
    def calibrate(self):
        if self._datatype == DataType.Price:
            returns = np.diff(np.log(self._data))
        else:
            returns = self._data
        drift = 260.0 * (np.mean(returns) + 0.5 * np.var(returns, ddof=1))
        vol =  np.sqrt(260.0) * np.std(returns,ddof=1)
        return GbmCalibrationResult(drift,vol)
