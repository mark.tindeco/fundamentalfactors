# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 16:00:09 2019

@author: Norberto
"""

import numpy as np
#import FactorModelCalibration

class FactorModel:
    
    def __init__(self,calibration):
        self._modeldrifts = 260.0 * calibration._betas @ calibration._drifts 
        self._factorcov =  260.0 * calibration._fundfactorcovariance
        self._factorvols = np.sqrt(np.diag(self._factorcov))
        self._factorcorr = self._factorcov / np.outer(self._factorvols, self._factorvols)
        self._residualdrifts = 260.0 * calibration._residualdrifts
        self._modelvols = calibration._betas * self._factorvols
        self._residualvols = np.sqrt(260.0 * calibration._residualvars)
        self._assetdim = len(self._modeldrifts)
        self._factordim = self._factorcov.shape[0]
        
    def logReturn(self,delta,shocks,idiosyncratic):
        randomcomponent = np.sqrt(delta) * (self._modelvols@shocks.T + (self._residualvols * idiosyncratic).T)
        return randomcomponent.add(delta * self._modeldrifts, axis='index')
        
    def stepUpdate(self,s,delta,shocks,idiosyncratic):
        #trials = s.shape[1]
        #drifts = np.tile(self._modeldrifts,trials).reshape(trials,self._assetdim).T
        #randomcomponent = np.sqrt(delta) * (self._modelvols@shocks.T + (self._residualvols * idiosyncratic).T)
        #logreturn = randomcomponent.add(delta * self._modeldrifts, axis='index')
        return s * np.exp(self.logReturn(delta,shocks,idiosyncratic)) #(drifts * delta + np.sqrt(delta) * randomcomponent)

    def MultiFactorPath(self,s,delta,number_of_steps,shocks,idiosyncratic):
        x = np.empty((len(s),number_of_steps + 1))
        x[:,0] = s
        y = s
        for j in range (0,number_of_steps):
            y = self.stepUpdate(y,delta,shocks[j,:],idiosyncratic[j,:])
            x[:, j + 1 ] = y
        return x
    
#calibration = FactorModelCalibration.calibrateFactorModel()
#factormodel = FactorModel(calibration)

#shocks = [np.random.multivariate_normal(np.zeros(factormodel._factordim), factormodel._factorcorr, 12) for j in range(100)]
#shocks = np.random.multivariate_normal(np.zeros(factormodel._factordim), factormodel._factorcorr, 1000)
#idioshocks = [np.random.multivariate_normal(np.zeros(factormodel._assetdim), np.diag(np.ones(factormodel._assetdim)), 12) for j in range(100)]
#idioshocks = np.random.multivariate_normal(np.zeros(factormodel._assetdim), np.diag(np.ones(factormodel._assetdim)), 1000) 
#s = np.ones(shape=(factormodel._assetdim,1000))
#sim = [factormodel.MultiFactorPath(np.ones(factormodel._assetdim),1/12,12,s,i) for (s,i) in zip(shocks,idioshocks)]
#sim = factormodel.stepUpdate(s, 1/12, shocks, idioshocks)
#simAtStep = np.array([x[:,1] for x in sim])
