from statsmodels.stats.weightstats import DescrStatsW
import pandas as pd, numpy as np

properties = pd.read_csv('AssetProperties.csv').set_index('RIC').sort_index()

divs = properties['Div']
caps = properties['Cap']
weights = caps / caps.sum()

bias = True # default for np.cov is bias = False
divs_weighted_mean = np.average(divs, weights=weights)
divs_weighted_std = np.sqrt(np.cov(divs, aweights=weights, bias=bias)) # default bias = False
normalised_div = (divs - divs_weighted_mean) / divs_weighted_std
normalised_div_weighted_mean = np.average(normalised_div, weights=weights)
normalised_div_weighted_std = np.sqrt(np.cov(normalised_div, aweights=weights, bias=bias))

#alternative = np.sqrt(np.average((normalised_div - weighted_normalised_mean)**2, weights=weights))

ddof = 0
# default for ddof is 0
# ddof=0 means dividing by N, ddof=1 means dividing by N-1
# weights should sum up to the number of observations
divs_weighted_stats = DescrStatsW(divs, weights=weights * 101, ddof=ddof)
divs_weighted_stats_mean = divs_weighted_stats.mean
divs_weighted_stats_std = divs_weighted_stats.std
normalised_div2 = (divs - divs_weighted_stats_mean) / divs_weighted_stats_std

normalised_weighted_stats = DescrStatsW(normalised_div2, weights=weights * 101, ddof=ddof)
normalised_weighted_stats_mean = normalised_weighted_stats.mean
normalised_weighted_stats_std = normalised_weighted_stats.std
