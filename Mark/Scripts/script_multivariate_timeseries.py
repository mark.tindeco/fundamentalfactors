from datasources import DatabaseFinancial
import numpy as np
from datetime import date

start_date = date(2014, 5, 19)
end_date = date(2019, 5, 21)
rics = np.array(['RIC', '.FTSE', 'GLEN.L', 'CCH.L', 'STAN.L', 'LAND.L', 'FERG.L', 'KGF.L', 'JMAT.L', 'SKG.L', 'SGRO.L', 'BNZL.L', 'SBRY.L', 'HSX.L', 'FLTRF.L', 'TW.L', 'CRH.L', 'ANTO.L', 'AHT.L', 'ABF.L', 'AVV.L', 'AV.L', 'BAES.L', 'BARC.L', 'BDEV.L', 'BKGH.L', 'BHPB.L', 'BLND.L', 'BT.L', 'BRBY.L', 'CCL.L', 'CNA.L', 'CPG.L', 'CRDA.L', 'DCC.L', 'EVRE.L', 'EXPN.L', 'FRES.L', 'HLMA.L', 'HRGV.L', 'HIK.L', 'IMB.L', 'INF.L', 'IHG.L', 'ICAG.L', 'ITRK.L', 'ITV.L', 'JD.L', 'LSE.L', 'MGGT.L', 'MRON.L', 'MNDI.L', 'MRW.L', 'NG.L', 'NXT.L', 'NMC.L', 'PSON.L', 'PSN.L', 'PHNX.L', 'POLYP.L', 'REL.L', 'RTO.L', 'RMV.L', 'RR.L', 'RBS.L', 'RSA.L', 'SGE.L', 'SDR.L', 'SMT.L', 'SVT.L', 'SN.L', 'SMDS.L', 'SMIN.L', 'SPX.L', 'SSE.L', 'SLA.L', 'SJP.L', 'TSCO.L', 'UU.L', 'VOD.L', 'WTB.L', 'WPP.L', 'AAL.L', 'AZN.L', 'BATS.L', 'BP.L', 'DGE.L', 'GSK.L', 'HSBA.L', 'LLOY.L', 'PRU.L', 'RB.L', 'RIO.L', 'ULVR.L'])
adjusted = True

with DatabaseFinancial('historical_prices', 3307) as historical_prices:
    asset_ids = historical_prices.get_asset_ids(rics=rics)
    ids_to_rics = {asset_id: ric for asset_id, ric in zip(asset_ids, rics) if asset_id is not None}
    prices = historical_prices.get_daily_close_prices(asset_ids, start_date, end_date, wide=True, adjusted=adjusted).droplevel('source_id', axis='columns').rename(mapper=ids_to_rics, axis='columns')

#prices['Date'] = prices.index.to_series().apply(lambda x: x.strftime('%d/%m/%Y'))
#prices.set_index('Date', inplace=True)
#prices.to_csv('prices.csv')
