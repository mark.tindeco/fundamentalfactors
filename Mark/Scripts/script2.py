from Mark.FactorModelCalibration import constructFactorMimickingPortfolios, dailyClosePricesFromDatabase
from Mark.FactorModel import FactorModel, AssetModel, ModelState
from datasources import DatabaseFinancial
import pandas as pd, numpy as np, datetime as dt, matplotlib.pyplot as plt

rics = ['STAN.L', 'CRDA.L', 'ANTO.L', 'MRW.L', 'EZJ.L', 'BNZL.L', 'SGE.L', 'SVT.L', 'BLND.L', 'ICAG.L', 'REL.L', 'SMIN.L', 'AZN.L', 'HSBA.L', 'WPP.L', 'FRES.L', 'SGRO.L', 'SJP.L', 'TW.L', 'AHT.L', 'HLMA.L', 'III.L', 'HIK.L', 'EVRE.L', 'CNA.L', 'BKGH.L', 'SMDS.L', 'NG.L', 'RB.L', 'SKG.L', 'MRON.L', 'HSX.L', 'NMC.L', 'BHPB.L', 'CPG.L', 'AV.L', 'DGE.L', 'INF.L', 'UU.L', 'WTB.L', 'PRU.L', 'MKS.L', 'IMB.L', 'EXPN.L', 'BRBY.L', 'SLA.L', 'GSK.L', 'LAND.L', 'BP.L', 'JE.L', 'ABF.L', 'AAL.L', 'ADML.L', 'CCL.L', 'RTO.L', 'RMV.L', 'SBRY.L', 'PHNX.L', 'FLTRF.L', 'IHG.L', 'BT.L', 'MNDI.L', 'BATS.L', 'PSON.L', 'CRH.L', 'SPX.L', 'PSN.L', 'RIO.L', 'JMAT.L', 'RDSb.L', 'CCH.L', 'RSA.L', 'RR.L', 'SN.L', 'SSE.L', 'ITV.L', 'HRGV.L', 'KGF.L', 'LLOY.L', 'MCRO.L', 'RBS.L', 'SDR.L', 'NXT.L', 'ITRK.L', 'BDEV.L', 'GLEN.L', 'VOD.L', 'BARC.L', 'RDSa.L', 'FERG.L', 'DLGD.L', 'BAES.L', 'DCC.L', 'ULVR.L', 'OCDO.L', 'LSE.L', 'TSCO.L', 'LGEN.L']
dateStart, dateEnd = dt.date(2014, 5, 18), dt.date(2019, 5, 21)
dateSimulationHorizon = dt.date(2019, 12, 31)
trials = 1000

def computeBetas(propertiesDataFile, rics):
    properties = pd.read_csv(propertiesDataFile).set_index('RIC').sort_index()
    betas = pd.DataFrame(index=rics)
    industries = properties['GICS Sector Name']
    sector_names = industries.dropna().unique().tolist()
    for sector_name in sector_names: betas[sector_name] = industries.apply(lambda sector: 1 if sector == sector_name else 0).reindex(rics)
    marketCaps = properties['Company Market Cap']
    weights = marketCaps / marketCaps.sum()
    divs = properties['Dividend yield'].fillna(value=0)
    divs_mean, divs_std = np.average(divs, weights=weights), np.sqrt(np.cov(divs, aweights=weights))
    betas['NormalisedDividendYield'] = (divs - divs_mean) / divs_std
    log_market_caps = marketCaps.apply(np.log)
    log_market_cap_mean, log_market_cap_std = np.average(log_market_caps), np.std(log_market_caps, ddof=1) # np.std(log_market_caps, ddof=1) = np.sqrt(np.cov(log_market_caps))
    betas['NormalisedLogMarketCaps'] = (log_market_caps - log_market_cap_mean) / log_market_cap_std
    log_pes = properties['P/E (Daily Time Series Ratio)'].apply(np.log).fillna(0)
    log_pes_mean, log_pes_std = np.average(log_pes), np.std(log_pes, ddof=1) # np.std(log_pes, ddof=1) = np.sqrt(np.cov(log_pes))
    betas['NormalisedLogPriceEarningsRatio'] = (log_pes - log_pes_mean) / log_pes_std
    betas.columns.names = ['Factors']; betas.index.names = ['RICs']; betas.sort_index(inplace=True)
    return betas

def plotFanChart(close_prices, quantiles, identifier, levels, rebase=False, ax=None, linewidth=1):
    if ax is None: ax = plt.figure().add_subplot(111, title=identifier)
    else: ax.set_title(None)
    scaling = 1.0 / close_prices.iloc[0][identifier] if rebase else 1.0
    ax = close_prices[identifier].multiply(scaling).plot(ax=ax, legend=identifier, linewidth=linewidth)
    df = pd.DataFrame(columns=levels)
    df.columns.name, df.index.name = 'Quantiles', 'Date'
    for date in list(quantiles.keys()): df.loc[date] = pd.Series(quantiles[date][identifier]) * scaling
    color = ax.lines[-1].get_color()
    colors = [color + alpha for alpha in ('1A', '33', '80', 'FF', '80', '33', '1A')]
    df.plot(ax=ax, legend=False, linewidth=linewidth, color=colors)
    ax.fill_between(df.index.tolist(), df[0.05], df[0.95], color=color, alpha = 5/50)
    ax.fill_between(df.index.tolist(), df[0.1], df[0.9], color=color, alpha = 10/50)
    ax.fill_between(df.index.tolist(), df[0.25], df[0.75], color=color, alpha = 25/50)
    return ax

def plotFactorFanCharts(factor, closePrices, quantiles, betas, levels, ax=None):
    factorIdenfifiers = betas[factor].index[betas[factor]==1].tolist()
    for identifier in factorIdenfifiers:
        linewidth = 3 if identifier in factorNames else 1
        ax = plotFanChart(closePrices, quantiles, identifier, levels, rebase=True, ax=ax, linewidth=linewidth)
    return ax

with DatabaseFinancial('historical_prices', 3307) as historical_prices:
    closePricesAssets = dailyClosePricesFromDatabase(historical_prices, rics, dateStart, dateEnd)
    closePricesIndex = dailyClosePricesFromDatabase(historical_prices, ['.FTSE'], dateStart, dateEnd).iloc[:, 0]
    logReturnsAssets = closePricesAssets.apply(np.log).diff().iloc[1:]
    logReturnsIndex = closePricesIndex.apply(np.log).diff().iloc[1:]
    logExcessReturnsAssets = logReturnsAssets.sub(logReturnsIndex, axis='index')

betasAssets = computeBetas('script2_AssetProperties.csv', rics)

logExcessReturnsCovariancesAssets = logExcessReturnsAssets.cov()
logExcessReturnsCovariancesInverseAssets = pd.DataFrame(np.linalg.inv(logExcessReturnsCovariancesAssets.values), index=logExcessReturnsCovariancesAssets.index, columns=logExcessReturnsCovariancesAssets.columns)

# My approach to constructing factors
factorMimickingPortfolios = constructFactorMimickingPortfolios(logExcessReturnsCovariancesInverseAssets, betasAssets, logExcessReturnsAssets)
# Note that assetBetas.T @ factorMimickingPortfolio is the identity matrix by construction
# Note that factorMimickingPortfolios.sum() is 1 for industries and 0 for normalised descriptors. I do not know why this is.
betasAssetsRecovered = logExcessReturnsCovariancesAssets @ factorMimickingPortfolios @ np.linalg.inv(factorMimickingPortfolios.T @ logExcessReturnsCovariancesAssets @ factorMimickingPortfolios); betasAssetsRecovered.columns = betasAssets.columns

logExcessReturnsFactors = logExcessReturnsAssets @ factorMimickingPortfolios
assetResiduals = logExcessReturnsAssets - logExcessReturnsFactors @ betasAssets.T

betasAssets['FTSE'] = 1
factorNames = list(betasAssets.columns)
betasFactors = pd.DataFrame(data=np.identity(len(factorNames)), index=factorNames, columns=factorNames)
betasFactors['FTSE'] = 1
betasAll = pd.concat([betasAssets, betasFactors], sort=False)

logExcessReturnsFactors['FTSE'] = logReturnsIndex
closePricesFactors = pd.DataFrame(columns=logExcessReturnsFactors.columns)
dateLast = dateStart + dt.timedelta(days=1)
closePricesFactors.loc[dateLast] = 1
closePricesFactors.loc[dateLast, 'FTSE'] = closePricesIndex.iloc[0]
for date in logExcessReturnsFactors.index:
    closePricesFactors.loc[date] = closePricesFactors.loc[dateLast].multiply(betasFactors.dot(logExcessReturnsFactors.loc[date]).apply(np.exp))
    dateLast = date

closePricesAll = closePricesAssets.merge(closePricesFactors, left_index=True, right_index=True)
spotPricesAll = closePricesAll.iloc[-1]

factorModel = FactorModel(mean=logExcessReturnsFactors.mean() * 260, cov=logExcessReturnsFactors.cov() * 260)
factorModelState = np.random.RandomState(seed=0)

residualVariancesAssets = assetResiduals.var(axis='index') * 260
residualVariancesFactors = pd.Series(0, index=betasAll.columns)
residualVariancesAll = residualVariancesAssets.append(residualVariancesFactors)
assetModel = AssetModel(factorModel, betasAll, residualVariancesAll)
assetModelState = ModelState(dateEnd, trials, spotPricesAll, seed=1)

assetQuantiles = {}
levels = (0.05, 0.1, 0.25, 0.5, 0.75, 0.9, 0.95)

while assetModelState.date < dateSimulationHorizon:
    assetModel.StepOneDay(factorModelState, assetModelState)
    assetQuantiles[assetModelState.date] = assetModelState.forwards.quantile(q=levels, numeric_only=False)

ax = plotFactorFanCharts(factorNames[10], closePricesAll, assetQuantiles, betasAll, levels)
