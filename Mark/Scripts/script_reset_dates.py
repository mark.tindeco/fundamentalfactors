from datasources import DatabaseFinancial
from asset import Asset
from datetime import timedelta

with DatabaseFinancial('historical_prices', 3307) as historical_prices:
    query = "SELECT `asset_id` FROM `assets`"
    results = historical_prices.fetch_results(query)
    for result in results:
        asset_id = result['asset_id']
        asset = Asset(asset_id, historical_prices)
        prices = historical_prices.get_daily_close_prices(asset_id, wide=True)
        start_date = prices.index[0] - timedelta(1)
        end_date = prices.index[-1]
        query = "UPDATE `assets` SET `start_date`='{}', `end_date`='{}' WHERE `asset_id`={}".format(start_date, end_date, asset_id)
        historical_prices.execute(query)
