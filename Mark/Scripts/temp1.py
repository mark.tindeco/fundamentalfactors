from datasources import DatabaseFinancial
from datasources import eikon
import pandas as pd

with DatabaseFinancial('historical_prices', 3307) as historical_prices:
    query = "SELECT `ric` FROM `assets`"
    rics = pd.DataFrame(historical_prices.fetch_results(query))['ric'].to_list()
    #all_symbology = eikon.get_symbology(symbols=rics, from_symbol_type='RIC')
    info = eikon.get_company_info(instruments=rics)
    
    for ric in rics:
        ric_symbols = info.loc[ric]
        name = ric_symbols['Company Common Name']
        name = name.replace("'", "\\\'")
        if isinstance(name, str): historical_prices.execute("UPDATE `assets` SET `name`='{}' WHERE `RIC`='{}'".format(name, ric))
