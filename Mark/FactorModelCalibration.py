import numpy as np

def dailyClosePricesFromDatabase(table, rics, start_date, end_date):
    asset_ids = table.get_asset_ids(rics=rics)
    ids_to_rics = {asset_id: ric for asset_id, ric in zip(asset_ids, rics) if asset_id is not None}
    prices = table.get_daily_close_prices(asset_ids, start_date, end_date, wide=True, adjusted=True).droplevel('source_id', axis='columns').rename(mapper=ids_to_rics, axis='columns')
    prices.columns.name = 'RIC'
    return prices.sort_index(axis='columns')

def constructFactorMimickingPortfolios(weights, betas, assetLogExcessReturns):
    """Use .mean(), .cov(), and .var() to compute moments of factor returns and residuals."""
    factorMimickingPortfolios = weights @ betas @ np.linalg.inv(betas.T @ weights @ betas); factorMimickingPortfolios.columns = betas.columns
    return factorMimickingPortfolios
