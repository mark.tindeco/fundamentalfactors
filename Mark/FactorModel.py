import pandas as pd, numpy as np, datetime as dt

class ShockGeneratorIndependent:

    def __init__(self, var: pd.Series):
        self.var = var

    def NormalTimeIncrement(self, delta, trials, randomState):
        return pd.DataFrame(randomState.normal(scale=self.var.apply(np.sqrt) * np.sqrt(delta), size=(trials, self.var.size)), columns=self.var.index)
    
    def Return(self, delta, trials, randomState): return np.exp(self.NormalTimeIncrement(delta, trials, randomState) - 0.5 * self.var * delta)

class ShockGeneratorCorrelated:

    def __init__(self, mean, cov):
        self.mean = mean
        self.cov = cov

    def NormalTimeIncrement(self, delta, trials, randomState):
        return pd.DataFrame(randomState.multivariate_normal(self.mean * delta, self.cov * delta, trials), columns=self.cov.columns)

class ModelState:

    def __init__(self, date, trials, initial_forwards: pd.Series, seed):
        self.date = date
        self.trials = trials
        self.forwards = pd.DataFrame(np.tile(initial_forwards, (self.trials, 1)), columns=initial_forwards.index)
        self.forwards.index.names = ['Trials']
        self.randomState = np.random.RandomState(seed)

class FactorModel:

    def __init__(self, mean, cov):
        self.cov = cov
        self.shockGenerator = ShockGeneratorCorrelated(mean, cov)

    def NormalTimeIncrement(self, trials, randomState):
        delta = 1/365.25
        return self.shockGenerator.NormalTimeIncrement(delta, trials, randomState)

class AssetModel:
    
    def __init__(self, factorModel, sensitivities, residualVariances):
        self.factorModel = factorModel
        self.sensitivities = sensitivities
        self.shockGenerator = ShockGeneratorIndependent(residualVariances)
        
    def StepOneDay(self, factorModelState, assetModelState):
        delta = 1/365.25
        trials = assetModelState.trials
        assetModelState.date += dt.timedelta(days=1)
        assetModelState.forwards *= np.exp(self.factorModel.NormalTimeIncrement(trials, factorModelState) @ self.sensitivities.T - 0.5 * np.diag(self.sensitivities @ self.factorModel.cov @ self.sensitivities.T) * delta)
        assetModelState.forwards *= self.shockGenerator.Return(delta, trials, assetModelState.randomState)
